from django.db.models import Max, Min, Avg

from pycha.models import Dish


def _serialize_dishes(dishes):
    return [{'id': dish.id, 'name': dish.name, 'description': dish.description, 'price': float(dish.price),
             'type': dish.type,
             'parameters':
                 [{'id': parameter.id, 'name': parameter.name} for parameter in dish.parameters.all()]
             }
            for dish in dishes]


def _serialize_address(address):
    return {'id': address.id, 'country': address.country, 'city': address.city, 'postCode': address.post_code,
            'street': address.street, 'doors': address.doors, 'client': address.client.user.username,
            'info': address.info}


def _serialize_order(order):
    return {'id': order.id, 'totalPrice': order.total_price, 'date': order.date}


def _serialize_suborders(suborders):
    return [{'id': suborder.id, 'restaurant': suborder.restaurant.name,
             'dishesQuantities': _serialize_dish_quantity(suborder.dishes_quantities.all())}
            for suborder in suborders]


def _serialize_dish(dish):
    return {'id': dish.id, 'name': dish.name, 'price': dish.price}
Dish.objects.filter(restaurant_id=1).aggregate(Max('price'))


def _serialize_dish_quantity(dish_quantities):
    return [{'id': dish_quantity.id, 'dish': _serialize_dish(dish_quantity.dish), 'quantity': dish_quantity.quantity,
             'parameter': dish_quantity.parameter.name, 'price': dish_quantity.quantity * dish_quantity.dish.price}
            for dish_quantity in dish_quantities]


def _serialize_restaurants(restaurants):
    return [{'id': restaurant.id, 'name': restaurant.name, 'phone': restaurant.phone,
             'radius': restaurant.delivery_radius, 'city': restaurant.address.city,
             'street': restaurant.address.street, 'doors': restaurant.address.doors,
             'noOfDishes': Dish.objects.filter(restaurant_id=restaurant.id).count(),
             'maxDishPrice': Dish.objects.filter(restaurant_id=restaurant.id).aggregate(Max('price'))['price__max'],
             'minDishPrice': Dish.objects.filter(restaurant_id=restaurant.id).aggregate(Min('price'))['price__min'],
             'avgDishPrice': Dish.objects.filter(restaurant_id=restaurant.id).aggregate(Avg('price'))['price__avg']}
            for restaurant in restaurants]
