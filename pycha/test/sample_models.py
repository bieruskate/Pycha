from django.contrib.auth.models import User
from django.urls import reverse

from pycha.models import Client, Address, Order, Restaurateur, Restaurant, Opinion, Parameter, Dish, QuantityDish, \
    Suborder, Message


def create_user(username, email, first_name, last_name, password='password'):
    user = User.objects.create_user(username, email, password)
    user.first_name = first_name
    user.last_name = last_name
    user.save()
    return user


def create_client(user, is_taster=False):
    client = Client()
    client.user = user
    client.is_taster = is_taster
    client.save()
    return client


def create_address(post_code, street, doors, client=None, city='Wrocław', country='Polska', info=None):
    address = Address()
    address.post_code = post_code
    address.street = street
    address.doors = doors
    address.client = client
    address.info = info
    address.country = country
    address.city = city
    address.save()
    return address


def create_order(client, address, total_price, date):
    order = Order()
    order.client = client
    order.address = address
    order.total_price = total_price
    order.date = date
    order.save()
    return order


def create_restaurateur(user, bank_account='12345678901234567890', phone='123456789', balance='123'):
    restaurateur = Restaurateur()
    restaurateur.user = user
    restaurateur.bank_account = bank_account
    restaurateur.phone = phone
    restaurateur.balance = balance
    restaurateur.save()
    return restaurateur


def create_restaurant(restaurateur, address, name, delivery_radius=5, phone='987654321',
                      description='Najlepsza restauracja w mieście'):
    restaurant = Restaurant()
    restaurant.restaurateur = restaurateur
    restaurant.address = address
    restaurant.name = name
    restaurant.phone = phone
    restaurant.delivery_radius = delivery_radius
    restaurant.description = description
    restaurant.save()
    return restaurant


def create_opinion(client, restaurant, weight, contents, date):
    opinion = Opinion()
    opinion.client = client
    opinion.restaurant = restaurant
    opinion.weight = weight
    opinion.contents = contents
    opinion.date = date
    opinion.save()
    return opinion


def create_parameter(name, type):
    parameter = Parameter()
    parameter.name = name
    parameter.type = type
    parameter.save()
    return parameter


def create_dish(restaurant, name, type, price, description, parameter):
    dish = Dish()
    dish.restaurant = restaurant
    dish.name = name
    dish.type = type
    dish.price = price
    dish.description = description
    dish.save()
    dish.parameters.add(parameter)
    dish.save()
    return dish


def create_quantity_dish(order, dish, quantity, parameter):
    quantity_dish = QuantityDish()
    quantity_dish.order = order
    quantity_dish.dish = dish
    quantity_dish.quantity = quantity
    quantity_dish.parameter = parameter
    quantity_dish.save()
    return quantity_dish


def create_suborder(order, restaurant, price, is_realised, dishes_quantities):
    suborder = Suborder()
    suborder.order = order
    suborder.restaurant = restaurant
    suborder.price = price
    suborder.is_realised = is_realised
    suborder.dishes_quantities = dishes_quantities
    suborder.save()
    return suborder


def create_message(sender, recipient, contents, date):
    message = Message()
    message.sender = sender
    message.recipient = recipient
    message.contents = contents
    message.date = date
    message.save()
    return message


def create_test_client(username, password):
    user_data = {'username': username, 'password': password}
    user = create_user(username, 'test@gmail.com', 'John', 'Surname', password)
    client = create_client(user)
    create_address('55-555', 'Jasna', 6, client, 'Konin')
    return user_data


def login_user(test, username, password):
    test.open(reverse('login'))
    test.wait_until_not_present('id_username')
    test.sel.find_element_by_id('id_username').send_keys(username)
    test.sel.find_element_by_id('id_password').send_keys(password)
    test.sel.find_element_by_id('id_login').click()


def test_if_dish_is_added_to_cart_data_set():
    rest_user = create_user('gesslerka', 'bessos@gmail.com', 'Magda', 'Gessler', 'alkoholizm')
    restaurateur = create_restaurateur(rest_user)
    rest_address = create_address('12-123', 'Sezamkowa', 64)
    parameter1 = create_parameter('Frytki', 1)
    restaurant = create_restaurant(restaurateur, rest_address, 'Ramenarnia')
    create_dish(restaurant, 'Inżynierskie pierogi', 'Danie główne', 13, 'Najlepsze pierogi na świecie',
                parameter1)
    create_dish(restaurant, 'Zupa przeznaczenia', 'Przystawka', 50, 'Zupa przeznaczona i naznaczona',
                parameter1)
