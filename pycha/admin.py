from django.contrib import admin

from pycha.models import Restaurant, Address, Dish, Parameter, Order, Suborder, Client, QuantityDish

admin.site.register(Restaurant)
admin.site.register(Address)
admin.site.register(Dish)
admin.site.register(Parameter)
admin.site.register(Order)
admin.site.register(Suborder)
admin.site.register(Client)
admin.site.register(QuantityDish)

