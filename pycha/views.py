import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views import View
from django.views.generic import ListView

from pycha.models import Restaurateur, Client, Restaurant, Dish, Order, Address, Suborder, QuantityDish, Parameter
from pycha.serializers import _serialize_dishes, _serialize_order, _serialize_suborders, _serialize_address, \
    _serialize_restaurants
from pycha_pl.settings import STATIC_ROOT


class IndexView(View):
    def get(self, request):
        return render(request, 'base.html')


class RestaurateurProcessView(View):
    def get(self, request):
        return render(request, 'restaurateur/add_restaurateur.html')

    def post(self, request):
        restaurateur = json.loads(request.body.decode())

        if User.objects.filter(username=restaurateur['name']).exists():
            new_restaurateur = Restaurateur()
            new_restaurateur.user = User.objects.get(username=restaurateur['name'])
            new_restaurateur.bank_account = restaurateur['account']
            new_restaurateur.phone = restaurateur['phone']
            new_restaurateur.balance = restaurateur['balance']
            new_restaurateur.save()

            return JsonResponse({'status': True})
        else:
            return JsonResponse({'status': False})


class RestaurateurView(ListView):
    model = Restaurateur
    template_name = 'restaurateur/restaurateur.html'


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html', {'login_status': True})

    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return render(request, 'login.html', {'login_status': False})


class Register(View):
    def get(self, request):
        return render(request, 'register.html')

    def post(self, request):
        user_data = json.loads(request.body.decode())

        username = user_data['name']
        password = user_data['password']
        email = user_data['email']
        user_type = user_data['type']
        try:
            new_user = User.objects.create_user(username=username, password=password, email=email)
            login(request, new_user)
        except IntegrityError:
            return JsonResponse({'status': False})

        if user_type == 'client':
            client = Client()
            client.user = new_user
            client.is_taster = False
            client.save()

        if user_type == 'restaurateur':
            restaurateur = Restaurateur()
            restaurateur.user = new_user
            restaurateur.balance = '0.0'
            restaurateur.bank_account = user_data['account']
            restaurateur.phone = user_data['phone']
            restaurateur.save()

        return JsonResponse({'status': True})


class RestaurantsView(View):
    def get(self, request):
        return render(request, 'restaurant/restaurants.html')


class RestaurantDetails(View):
    def get(self, request, restaurant_id):
        restaurant = Restaurant.objects.filter(pk=restaurant_id).first()
        return render(request, 'restaurant/details.html', {'restaurant': restaurant})


class RestaurantsProcessView(View):
    def get(self, request):
        restaurants = _serialize_restaurants(Restaurant.objects.all())
        return JsonResponse(restaurants, safe=False)


class DishesProcessView(View):
    def get(self, request, restaurant_id):
        dishes = _serialize_dishes(Dish.objects.filter(restaurant=restaurant_id))
        return JsonResponse(dishes, safe=False)

    @method_decorator(login_required(login_url='/login/'))
    def post(self, request, dish_id):

        dish_param = json.loads(request.body.decode())
        parameter = Parameter.objects.get(pk=dish_param['id'])

        dish = Dish.objects.get(pk=dish_id)

        client = Client.objects.get(user=request.user)
        client_address = get_object_or_404(Address, client=client)

        if not client_address:
            return JsonResponse({'status': False})

        order = Order.objects.filter(client=client)

        if not order.exists():
            order = Order.objects.create(client=client, address=client_address, total_price=0, date=timezone.now())
        else:
            order = order.first()

        suborder = Suborder.objects.filter(order=order, restaurant=dish.restaurant)
        if not suborder.exists():
            suborder = Suborder.objects.create(order=order, restaurant=dish.restaurant, price=0, is_realised=False)
        else:
            suborder = suborder.first()

        quantity_dish = QuantityDish.objects.filter(order=order, dish=dish, parameter=parameter)
        if not quantity_dish.exists():
            quantity_dish = QuantityDish.objects.create(dish=dish, order=order, quantity=1, parameter=parameter)
        else:
            quantity_dish = quantity_dish.first()
            quantity_dish.quantity += 1
            quantity_dish.parameter = parameter
            quantity_dish.save()

        suborder.price += dish.price
        suborder.save()

        order.total_price += dish.price
        order.save()

        suborder.dishes_quantities.add(quantity_dish)
        suborder.save()

        return JsonResponse({'status': True})


class CartProcessView(View):
    def get(self, request):
        client = Client.objects.get(user=request.user)
        order = Order.objects.filter(client=client)
        suborders = Suborder.objects.filter(order=order.first())

        if not order.exists():
            return JsonResponse({'exist': False})

        return JsonResponse({'order': _serialize_order(order.first()),
                             'suborders': _serialize_suborders(suborders)},
                            safe=False)

    def delete(self, request, quantity_dish_id, order_id):
        qdish = QuantityDish.objects.get(id=quantity_dish_id)
        qdish.quantity -= 1

        if qdish.quantity == 0:
            qdish.delete()
        else:
            qdish.save()

        order = Order.objects.get(id=order_id)
        order.total_price -= qdish.dish.price

        if order.total_price > 0:
            order.save()
        else:
            order.delete()
            return JsonResponse({'deleted': True})

        return JsonResponse({'deleted': False})


class CartView(View):
    @method_decorator(login_required(login_url='/login/'))
    def get(self, request):

        client = Client.objects.get(user=request.user)
        order = Order.objects.filter(client=client)

        if not order.exists():
            return render(request, 'cart.html')
        else:
            order = order.first()

        suborders = Suborder.objects.filter(order=order)

        return render(request, 'cart.html', {'suborders': suborders, 'order': order})


class ProfileView(View):
    @method_decorator(login_required(login_url='/login/'))
    def get(self, request):
        return render(request, 'client/profile.html')

    def post(self, request):
        data = json.loads(request.body.decode())

        user = request.user
        user.username = data['name']
        user.email = data['email']

        user.first_name = data['firstName']
        user.last_name = data['lastName']

        if 'password' in data:
            if data['password'] != '':
                user.set_password(data['password'])
        try:
            user.save()
        except IntegrityError:
            return JsonResponse({'exists': True})

        return HttpResponse()

    def patch(self, request):
        address = json.loads(request.body.decode())

        country = address['country']
        city = address['city']
        street = address['street']
        doors = address['doors']
        post_code = address['postCode']

        info = address['info'] if 'info' in address else ''

        if 'id' in address:
            address_id = address['id']
            address_old = Address.objects.get(pk=address_id)

            address_old.country = country
            address_old.city = city
            address_old.street = street
            address_old.doors = doors
            address_old.post_code = post_code
            address_old.info = info

            address_old.save()

        else:
            address, created = Address.objects.get_or_create(country=country,
                                                             city=city,
                                                             street=street,
                                                             doors=doors,
                                                             post_code=post_code,
                                                             info=info)
            client = Client.objects.get(user=request.user)
            address.client = client
            address.save()

        return HttpResponse()


def is_user_client(user):
    return Client.objects.filter(user=user).exists()


class ProfileProcessView(View):
    def get(self, request):
        user = request.user

        if is_user_client(user):
            client = Client.objects.get(user=user)

            if Address.objects.filter(client=client).exists():
                address = Address.objects.get(client=client)
                return JsonResponse(_serialize_address(address))

        return HttpResponse()


class UploadView(View):
    def get(self, request):
        return render(request, 'upload.html')

    def post(self, request):
        file = request.FILES['file']
        with open(STATIC_ROOT + 'uploads/' + file._name, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)

        return JsonResponse({'status': True})
