from django.apps import AppConfig


class PychaConfig(AppConfig):
    name = 'pycha'
