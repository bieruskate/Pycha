from django.contrib.auth.models import User
from django.db import models


class Client(models.Model):
    def __str__(self):
        return 'Client: {}'.format(self.user)

    user = models.ForeignKey(User)
    is_taster = models.BooleanField()


class Address(models.Model):
    def __str__(self):
        return 'Address: {} {}'.format(self.street, self.doors)

    country = models.CharField(max_length=15, blank=True, default='Polska')
    city = models.CharField(max_length=30, blank=True, default='Wrocław')
    post_code = models.CharField(max_length=6)
    street = models.CharField(max_length=20)
    doors = models.PositiveSmallIntegerField()
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True)
    info = models.CharField(max_length=45, blank=True, null=True)


class Order(models.Model):
    def __str__(self):
        return 'Order of : {}'.format(self.client.user)

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.PROTECT)
    total_price = models.DecimalField(max_digits=7, decimal_places=2)
    date = models.DateTimeField()


class Restaurateur(models.Model):
    def __str__(self):
        return 'Restaurateur: {}#{}'.format(self.user.username, self.id)

    user = models.ForeignKey(User)
    bank_account = models.CharField(max_length=20)
    phone = models.CharField(max_length=9)
    balance = models.DecimalField(max_digits=7, decimal_places=2)


class Restaurant(models.Model):
    def __str__(self):
        return 'Restaurant: {}'.format(self.name)

    restaurateur = models.ForeignKey(Restaurateur, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=9)
    delivery_radius = models.PositiveSmallIntegerField()
    description = models.CharField(max_length=700)


class Opinion(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    weight = models.FloatField()
    contents = models.CharField(max_length=300)
    date = models.DateTimeField()


class Parameter(models.Model):
    def __str__(self):
        return 'Parameter: #{} {}'.format(self.type, self.name)

    name = models.CharField(max_length=30)
    type = models.PositiveSmallIntegerField()


class Dish(models.Model):
    def __str__(self):
        return 'Dish: {}'.format(self.name)

    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name = models.CharField(max_length=45)
    type = models.CharField(max_length=20)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    description = models.CharField(max_length=100, blank=True, null=True)
    parameters = models.ManyToManyField(Parameter)


class QuantityDish(models.Model):
    def __str__(self):
        return '{} {} {}'.format(self.dish.name, self.parameter, self.quantity)

    order = models.ForeignKey(Order)
    dish = models.ForeignKey(Dish)
    quantity = models.PositiveSmallIntegerField()
    parameter = models.ForeignKey(Parameter)


class Suborder(models.Model):
    def __str__(self):
        return 'Suborder of : {}'.format(self.order)

    order = models.ForeignKey(Order)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    is_realised = models.BooleanField()
    dishes_quantities = models.ManyToManyField(QuantityDish)


class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sender_of_the_message')
    recipient = models.ForeignKey(User, blank=True, null=True, related_name='recipient_of_the_message')
    contents = models.CharField(max_length=300)
    date = models.DateTimeField()
