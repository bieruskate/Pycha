from time import sleep

from django.test import LiveServerTestCase
from django.urls import reverse
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Firefox
from selenium.webdriver.support.ui import WebDriverWait

from pycha.models import Dish, User, Address, QuantityDish, Restaurateur
from pycha.test.sample_models import create_test_client, login_user, test_if_dish_is_added_to_cart_data_set, \
    create_user, \
    create_client, create_restaurant, create_restaurateur, create_address


class FeatureTestCase(LiveServerTestCase):
    def setUp(self):
        self.sel = Firefox()
        self.wait = WebDriverWait(self.sel, 10)
        super(FeatureTestCase, self).setUp()

    def tearDown(self):
        self.sel.quit()
        super(FeatureTestCase, self).tearDown()

    def open(self, url):
        self.sel.get("%s%s" % (self.live_server_url, url))

    def wait_until_not_present(self, id):
        self.wait.until(lambda driver: driver.find_element_by_id(id))

    def check_exists_by_id(self, id):
        try:
            self.sel.find_element_by_id(id)
        except NoSuchElementException:
            return False
        return True

    def test_if_client_register_process_is_ok(self):
        self.open(reverse('register'))

        self.wait_until_not_present('clientForm')
        self.sel.find_element_by_id('name').send_keys('username')
        self.sel.find_element_by_id('id_password').send_keys('password')
        self.sel.find_element_by_id('id_email').send_keys('test_email@io.pwr')

        submit = 'register-submit-btn'
        submit = self.sel.find_element_by_id(submit)
        submit.click()

        sleep(1)

        user = User.objects.filter(username='username')
        self.assertTrue(user.exists())

    def test_if_restaurateur_register_process_is_ok(self):
        test_restaurateur = {'username': 'Magda',
                             'password': 'Gessler',
                             'email': 'bessossss@gmail.com',
                             'account': '01234567890123456789',
                             'phone': '123456789'}

        self.open(reverse('register'))
        self.wait_until_not_present('restaurateurRadio')
        self.sel.find_element_by_id('restaurateurRadio').click()

        self.sel.find_element_by_id('restaurateurForm')
        self.sel.find_element_by_id('name').send_keys(test_restaurateur['username'])
        self.sel.find_element_by_id('id_password').send_keys(test_restaurateur['password'])
        self.sel.find_element_by_id('id_email').send_keys(test_restaurateur['email'])
        self.sel.find_element_by_id('account').send_keys(test_restaurateur['account'])
        self.sel.find_element_by_id('phone').send_keys(test_restaurateur['phone'])
        self.sel.find_element_by_id('register-submit-btn').click()

        sleep(1)

        user = User.objects.get(username=test_restaurateur['username'])
        restaurateur = Restaurateur.objects.filter(user=user)
        self.assertTrue(restaurateur.exists())

    def test_if_login_failed(self):
        create_user('meehow', 'meehow@gmail.com', 'Michał', 'Bieroński')
        self.open(reverse('login'))
        self.wait_until_not_present('id_username')
        self.sel.find_element_by_id('id_username').send_keys('username')
        self.sel.find_element_by_id('id_password').send_keys('passwodko')
        self.sel.find_element_by_id('id_login').click()
        self.wait_until_not_present('login_failed_alert')
        self.assertTrue(self.check_exists_by_id('login_failed_alert'))

    def test_if_dish_is_added_to_cart(self):
        test_client = create_test_client('suchygrzech', 'password')
        login_user(self, test_client['username'], test_client['password'])
        test_if_dish_is_added_to_cart_data_set()

        self.wait_until_not_present('restaurantsNav')
        # sleep(1)
        self.sel.find_element_by_id('restaurantsNav').click()

        self.wait_until_not_present('id_restaurant')
        self.sel.find_element_by_id('id_restaurant').click()

        self.wait_until_not_present('dish1')
        self.sel.find_element_by_id('dish1').click()
        sleep(1)
        self.wait_until_not_present('closeModal')
        self.sel.find_element_by_id('closeModal').click()
        sleep(1)
        self.wait_until_not_present('dish2')
        self.sel.find_element_by_id('dish2').click()
        sleep(1)
        self.wait_until_not_present('closeModal')
        self.sel.find_element_by_id('closeModal').click()

        dish1 = Dish.objects.get(name='Inżynierskie pierogi')
        quant_dish1 = QuantityDish.objects.filter(dish=dish1)
        self.assertTrue(quant_dish1.exists())

        dish2 = Dish.objects.get(name='Zupa przeznaczenia')
        quant_dish2 = QuantityDish.objects.filter(dish=dish2)
        self.assertTrue(quant_dish2.exists())

    def test_if_filling_address_form_is_ok(self):
        data_to_test = {'country': 'Czechy',
                        'city': 'Praga',
                        'street': 'Czeska',
                        'doors': 9,
                        'postCode': '12-564'}

        user = create_user('suchygrzech', 'test@gmail.com', 'Grzegorz', 'Suszka', 'password')
        client = create_client(user)
        login_user(self, 'suchygrzech', 'password')

        self.wait_until_not_present('profileNav')
        self.sel.find_element_by_id('profileNav').click()

        self.wait_until_not_present('addressForm')
        self.sel.find_element_by_id('country').send_keys(data_to_test['country'])
        self.sel.find_element_by_id('city').send_keys(data_to_test['city'])
        self.sel.find_element_by_id('street').send_keys(data_to_test['street'])
        self.sel.find_element_by_id('doors').send_keys(data_to_test['doors'])
        self.sel.find_element_by_id('postCode').send_keys(data_to_test['postCode'])
        self.sel.find_element_by_id('addressFormBtn').click()

        sleep(1)

        address = Address.objects.filter(client=client)
        self.assertEqual(address.get().country, data_to_test['country'])
        self.assertEqual(address.get().city, data_to_test['city'])
        self.assertEqual(address.get().street, data_to_test['street'])
        self.assertEqual(address.get().doors, data_to_test['doors'])
        self.assertEqual(address.get().post_code, data_to_test['postCode'])

    def test_if_restaurants_filter_works(self):
        self.create_test_restaurants()
        self.open(reverse('restaurants'))

        self.wait_until_not_present('filter')
        self.sel.find_element_by_id('filter').send_keys('bul')

        sleep(1)
        self.assertFalse(self.check_exists_by_id('restaurant-1'))
        self.assertFalse(self.check_exists_by_id('restaurant-2'))
        self.assertFalse(self.check_exists_by_id('restaurant-3'))
        self.assertFalse(self.check_exists_by_id('restaurant-4'))
        self.assertFalse(self.check_exists_by_id('restaurant-6'))

        self.assertTrue(self.check_exists_by_id('restaurant-5'))
        self.assertTrue(self.check_exists_by_id('restaurant-7'))

    def test_if_change_username_works(self):
        client = create_test_client('suchygrzech', 'michal123')
        login_user(self, client['username'], client['password'])

        self.wait_until_not_present('profileNav')
        self.sel.find_element_by_id('profileNav').click()

        self.wait_until_not_present('profileForm')
        name = self.sel.find_element_by_id('name')
        name.clear()
        name.send_keys('suchymichal')
        self.sel.find_element_by_id('submitBtn').click()

        sleep(2)
        user = User.objects.filter(username='suchymichal')
        self.assertTrue(user.exists())

    def create_test_restaurants(self):
        user = create_user('drzewko_csa', 'zak_the_best@gmail.com', 'Janush', 'Biernat')
        rest = create_restaurateur(user)
        address = create_address('00-000', 'Jana Pawła 3', 5)
        create_restaurant(rest, address, 'Bazylia')
        create_restaurant(rest, address, 'Pod Ptakiem')
        create_restaurant(rest, address, 'Usta Polaka')
        create_restaurant(rest, address, 'Jabłkowy Gniot')
        create_restaurant(rest, address, 'Cebulaż')
        create_restaurant(rest, address, 'Samsung')
        create_restaurant(rest, address, 'Bulimiarz')
