displayErrorModal = function (heading, body) {
    $('#modalErrorHeading').text(heading);
    $('#modalErrorBody').text(body);
    $("#errorModal").modal()
};

displayDefaultModal = function (heading, body) {
    $('#modalDefaultHeading').text(heading);
    $('#modalDefaultBody').text(body);
    $("#defaultModal").modal()
};