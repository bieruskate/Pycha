var app = angular.module('restaurant', []).config(function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

app.controller('restaurantCtrl', function ($scope, $http) {

    $scope.sortTypes = [
        {text: 'Nazwa restauracji', value: 'name'},
        {text: 'Promień dowozu', value: 'radius'},
        {text: 'Miejscowość', value: 'city'},
        {text: 'Najniższe ceny', value: 'minDishPrice'},
        {text: 'Najwyższe ceny', value: '-maxDishPrice'},
        {text: 'Średnia cen', value: 'avgDishPrice'},
        {text: 'Liczba dań', value: '-noOfDishes'}
    ];

    $scope.mySortType = $scope.sortTypes[0];

    $scope.restaurants = [];

    $scope.getRestaurants = function () {
        $http.get("/restaurants/json/")
            .success(function (data) {
                console.log(data);
                $scope.restaurants = data;

                angular.forEach($scope.restaurants, function (restaurant, index) {
                    restaurant.url = '/static/images/restaurants/' + restaurant.id + '.jpg';
                    restaurant.detailsUrl = '/restaurants/' + restaurant.id;
                });

                return data;
            });
    };
});

app.controller('detailsCtrl', function ($scope, $http) {
    var restaurantId;
    $scope.dishes = [];

    $scope.sortTypes = [
        {text: 'Nazwa dania', value: 'name'},
        {text: 'Cena', value: 'radius'}
    ];

    $scope.mySortType = $scope.sortTypes[0];

    $scope.getDishes = function (restaurant_id) {
        $http.get('/restaurants/json/' + restaurant_id)
            .success(function (data) {
                console.log(data);
                $scope.dishes = data;
                restaurantId = restaurant_id;

                angular.forEach($scope.dishes, function (dish, index) {
                    dish.chosenParam = dish.parameters[0];
                });

                return data;
            })
    };

    $scope.addDishToCart = function (dishId, chosenParam) {

        $http.post("/cart/add_dish/" + dishId + "/", JSON.stringify(chosenParam))
            .success(function (data) {
                if (data.status)
                    displayDefaultModal('Dodano produkt', 'Produkt został dodany do koszyka.');
                else
                    window.location.href = '/login'
            })
            .error(function (data) {
                $('#modalButtonUrl').attr('href', '/profile/').text('Do profilu').removeAttr('style');
                displayErrorModal('Błąd', 'Nie udało się dodać produktu do koszyka. Adres klienta nie został ustawiony.');
            })
    }

});
