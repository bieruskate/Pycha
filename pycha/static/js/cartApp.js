var app = angular.module('cartApp', []).config(function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.controller('cartCtrl', function ($scope, $http) {

    $scope.getCart = function () {
        $http.get('/cart/json/')
            .success(function (data) {
                console.log(data);
                $scope.order = data.order;
                $scope.suborders = data.suborders;
                return data;
            })
    };

    $scope.removeProduct = function (quantityDishId, orderId) {
        console.log('removing');
        $http.delete('remove_dish/' + quantityDishId + '/' + orderId + '/')
            .success(function (data) {
                $scope.getCart();

                if (data.deleted)
                    location.reload();
            })

    }

});