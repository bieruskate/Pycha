var app = angular.module('formApp', []).config(function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.controller('formCtrl', function ($scope, $http, $window) {
    $scope.addRestaurateur = function () {
        $http.post("/restaurateur/add/new", JSON.stringify($scope.restaurateur))
            .success(function (data) {
                if (data.status)
                    $window.location.href = '/restaurateur';
                else
                    displayErrorModal('Brak restauratora', 'Wybrany restaurator nie istnieje')
            });
    };

    $scope.registerUser = function () {
        $http.post("/register/", JSON.stringify($scope.user))
            .success(function (data) {
                if (data.status)
                    $window.location.href = '/';
                else
                    displayErrorModal('Nick zajęty', 'Użytkownik już istnieje, wybierz inny nick')
            });
    };

    $scope.editProfile = function () {
        $http.post("/profile/update/", JSON.stringify($scope.user))
            .success(function (data) {
                if (data.exists)
                    displayErrorModal('Nick zajęty', 'Użytkownik już istnieje, wybierz inny nick');
                else
                    displayDefaultModal('Zaktualizowano', 'Profil użytkownika został zaktualizowany');
            });

    };

    $scope.getClientAddress = function () {
        $http.get("/profile/client/address/")
            .success(function (data) {
                console.log(data);
                $scope.address = data;
            });

    };

    $scope.editAddress = function () {
        console.log($scope.address);
        $http.patch('/profile/update/address/', JSON.stringify($scope.address))
            .success(function (data) {
                displayDefaultModal('Zaktualizowano', 'Adres klienta został zaktualizowany');
            })
    };


});