var uploadApp = angular.module('uploadApp', []).config(function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

uploadApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

uploadApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function () {
            })
            .error(function () {
            });
    }
}]);

uploadApp.controller('uploadCtrl', ['$scope', 'fileUpload', function ($scope, fileUpload) {

    $scope.resetForm = function () {
        $('input').val("");
    };

    $scope.uploadFile = function () {
        var file = $scope.myFile;
        console.log('file is ');
        console.log(file);
        var uploadUrl = "/upload/";
        fileUpload.uploadFileToUrl(file, uploadUrl);
    };

}]);