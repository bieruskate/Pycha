"""pycha_pl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import logout

from pycha import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^admin/', admin.site.urls),

    url(r'^restaurateur/$', views.RestaurateurView.as_view()),
    url(r'^restaurateur/add$', views.RestaurateurProcessView.as_view()),
    url(r'^restaurateur/add/new$', views.RestaurateurProcessView.as_view()),

    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^register/$', views.Register.as_view(), name='register'),

    url(r'^restaurants/$', views.RestaurantsView.as_view(), name='restaurants'),
    url(r'^restaurants/(?P<restaurant_id>[0-9]+)$', views.RestaurantDetails.as_view(), name='restaurant details'),

    url(r'^restaurants/json/$', views.RestaurantsProcessView.as_view(), name='get restaurants'),
    url(r'restaurants/json/(?P<restaurant_id>[0-9]+)/$', views.DishesProcessView.as_view(), name='get dishes'),

    url(r'^cart/$', views.CartView.as_view(), name='cart'),
    url(r'^cart/add_dish/(?P<dish_id>[0-9]+)/$', views.DishesProcessView.as_view(), name='add dish to cart'),


    url(r'^cart/json/$', views.CartProcessView.as_view(), name='get cart'),
    url(r'^cart/remove_dish/(?P<quantity_dish_id>[0-9]+)/(?P<order_id>[0-9]+)/$', views.CartProcessView.as_view(),
        name='remove dish from cart'),

    url(r'^profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^profile/update/$', views.ProfileView.as_view(), name='profile update'),
    url(r'^profile/update/address/$', views.ProfileView.as_view(), name='address update'),
    url(r'^profile/client/address/$', views.ProfileProcessView.as_view(), name='client address'),

    url(r'^upload/$', views.UploadView.as_view())
]
