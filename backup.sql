-- MySQL dump 10.16  Distrib 10.1.20-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.1.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add user',2,'add_user'),(5,'Can change user',2,'change_user'),(6,'Can delete user',2,'delete_user'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add permission',4,'add_permission'),(11,'Can change permission',4,'change_permission'),(12,'Can delete permission',4,'delete_permission'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add suborder',7,'add_suborder'),(20,'Can change suborder',7,'change_suborder'),(21,'Can delete suborder',7,'delete_suborder'),(22,'Can add client',8,'add_client'),(23,'Can change client',8,'change_client'),(24,'Can delete client',8,'delete_client'),(25,'Can add message',9,'add_message'),(26,'Can change message',9,'change_message'),(27,'Can delete message',9,'delete_message'),(28,'Can add restaurant',10,'add_restaurant'),(29,'Can change restaurant',10,'change_restaurant'),(30,'Can delete restaurant',10,'delete_restaurant'),(31,'Can add restaurateur',11,'add_restaurateur'),(32,'Can change restaurateur',11,'change_restaurateur'),(33,'Can delete restaurateur',11,'delete_restaurateur'),(34,'Can add parameter',12,'add_parameter'),(35,'Can change parameter',12,'change_parameter'),(36,'Can delete parameter',12,'delete_parameter'),(37,'Can add order',13,'add_order'),(38,'Can change order',13,'change_order'),(39,'Can delete order',13,'delete_order'),(40,'Can add opinion',14,'add_opinion'),(41,'Can change opinion',14,'change_opinion'),(42,'Can delete opinion',14,'delete_opinion'),(43,'Can add dish',15,'add_dish'),(44,'Can change dish',15,'change_dish'),(45,'Can delete dish',15,'delete_dish'),(46,'Can add address',16,'add_address'),(47,'Can change address',16,'change_address'),(48,'Can delete address',16,'delete_address'),(58,'Can add quantity dish',20,'add_quantitydish'),(59,'Can change quantity dish',20,'change_quantitydish'),(60,'Can delete quantity dish',20,'delete_quantitydish');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (2,'pbkdf2_sha256$30000$FRkml03zwUIv$FkdPGLJwTHTxXs5i7cUmTQORGn+UTwg9cNgSJJu1CWg=','2017-01-04 00:42:39.349346',0,'janusz','','','janusz@gmail.com',0,1,'2017-01-04 00:42:39.152394'),(3,'pbkdf2_sha256$30000$FFOs2LddALnZ$NzID/C3I/SH5bOI+hCOVwcapEWM56aG9Dsk8THGj9tA=','2017-01-04 10:23:02.000000',0,'januszek','Janusz','Więcławski','janusz@gmail.com',0,1,'2017-01-04 10:23:01.000000'),(4,'pbkdf2_sha256$30000$w5dIY792RQPG$UWKYpU+nY6y5RJu8A7pkUpAw6umJLb/wMUYGlXRWQHg=','2017-01-04 10:38:43.796788',0,'andrew89','','','andrew@gmail.com',0,1,'2017-01-04 10:38:43.614794'),(5,'pbkdf2_sha256$30000$3riQqqVwWDhS$q9lEkjyB0gmjtttdOaMJJ5aBH4wjfL6OedtOB7j0zO4=','2017-01-04 19:39:34.000000',0,'madanne','Anna','Dymna','anna.siwik@onet.eu',0,1,'2017-01-04 19:39:34.000000'),(6,'pbkdf2_sha256$30000$cszNDbcrO8z8$NtGle1AKGwy/SApXHHphR9sCNq9niFVpBFQ62EgwJjU=','2017-01-09 18:42:40.214891',0,'arkanum','','','arkanum@gmail.com',0,1,'2017-01-09 18:42:40.074888'),(7,'pbkdf2_sha256$30000$cJj8dUWGy3C4$NgUFgfeWT4Zo24eIui0y4c4xtuSzVVzNURcU9OnxQN8=','2017-01-10 17:17:16.758169',0,'kacper','','','dfsfds@gmail.com',0,1,'2017-01-10 17:17:16.580630'),(8,'pbkdf2_sha256$30000$W5x9vZ2u6B3G$Ow4Gp27BabCvRI+HY9TNC8jt9mzLSkwWcNo+smoIG3Q=','2017-01-10 18:44:50.594953',0,'matbur','','','marburcf@gmail.com',0,1,'2017-01-10 18:44:34.677690'),(9,'pbkdf2_sha256$30000$tgrP0vOAZPON$5RNTTC3TjBLZhyav5SygUknf26BHep8buqzZh42xSOQ=','2017-01-12 18:03:01.828712',0,'mati','','','mati@gmail.com',0,1,'2017-01-12 18:03:01.700798'),(10,'pbkdf2_sha256$30000$tC9f16P6XuzF$4RuN27KUbj37EoDmORiZSHupJNUND01DdVOXl4MuQvA=','2017-01-15 18:08:52.744632',0,'ktos','','','janusz@gmail.com',0,1,'2017-01-15 18:08:52.552811'),(11,'janek','2017-01-21 18:35:56.603173',0,'janiezbedny','','','elosmiecie@gmail.com',0,1,'2017-01-15 18:10:13.330550'),(12,'pbkdf2_sha256$30000$fNS4xCj54VkO$OESfUvvulcdYtiqzdbK6O0BktF3BHHdQifTWW7G73k4=','2017-01-15 18:51:30.397101',0,'lol','','','janusz@gmail.com',0,1,'2017-01-15 18:51:30.132835'),(13,'pbkdf2_sha256$30000$eP0oSgbiAPtH$L+uhiCEL/RDwJhGZMXUr0FK95siVSdmmzFs699F9Uvc=','2017-01-15 19:50:24.548640',0,'aaa','','','amidn@fm.com',0,1,'2017-01-15 19:50:24.252405'),(14,'pbkdf2_sha256$30000$SRoTsjBLB8ZS$TNqSdPg81/Pj9thIf7KOA1W4qShVDpxP5H51yCuwXTs=','2017-01-21 19:16:09.766073',1,'admin','','','amidn@fm.com',1,1,'2017-01-21 19:15:09.339841'),(15,'pbkdf2_sha256$30000$NGrBje35vbu9$R2lGA4Oyxq0N90lJ1eFVSr1+2RGW1e1Q02yq2Le0uvg=','2017-01-21 20:47:41.790832',0,'marekpiasecki','','','marek94@gmail.com',0,1,'2017-01-21 19:18:55.427006'),(17,'pbkdf2_sha256$30000$FVcdvs98APP8$8Qowc3uLI0b/vJx5T0vycXG2+9vzrwhYjd9zDEqQ2iA=','2017-01-21 20:57:04.704431',1,'abc','','','',1,1,'2017-01-21 20:52:19.296279'),(19,'pbkdf2_sha256$30000$yvuogzm36Wte$mrFGnTGWAGytgwuGguqezNn1q00cmOT2xQuXynMlKWk=','2017-01-22 14:24:15.603378',1,'bieronsk','','','piasekelo@gmail.com',1,1,'2017-01-21 20:58:40.566421'),(20,'pbkdf2_sha256$30000$HbbLSELMSeAE$2GgjcX0DPZI11ZUXSzn2CyVE0G44WUExqiQ7qIb37TU=','2017-01-21 22:16:40.095034',0,'superuser','','','noelo@gmail.com',0,1,'2017-01-21 22:08:30.946537'),(21,'pbkdf2_sha256$30000$GcTYPTK5D7e4$UJ3bOt9+tPdLjgvmtAJZswC4E4F2FhlpZouWmqjRwos=','2017-01-21 23:20:40.530509',0,'grzechu','','','grzechu@gmail.com',0,1,'2017-01-21 23:20:40.290176'),(24,'pbkdf2_sha256$30000$WhPeya2wDNCz$TR+JUCQE3r4z5uO7/mM5gHTxAi6Fqztkeu0ToLiV8zA=','2017-01-21 23:30:13.615911',0,'abcd','','','abc@gmail.com',0,1,'2017-01-21 23:30:13.444703'),(25,'pbkdf2_sha256$30000$2LTAqch6tNSL$1K6qaxNmwvj3wEyaokblABtQEfdMqNGv8IQs+SV1caM=','2017-01-22 14:16:21.379967',0,'aniabierka','','','aniabierka@gmail.com',0,1,'2017-01-22 14:16:21.145573');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (57,'2017-01-21 19:17:44.817994','3','Address: Pabianicka 13',2,'[{\"changed\": {\"fields\": [\"client\"]}}]',16,14),(58,'2017-01-21 19:18:07.224320','1','bieronsk',3,'',2,14),(61,'2017-01-21 20:54:19.091646','3','Address: Pabianicka 13',2,'[{\"changed\": {\"fields\": [\"client\"]}}]',16,17),(62,'2017-01-21 20:54:39.885490','8','Client: bieronsk',3,'',8,17),(63,'2017-01-21 20:54:57.663608','16','bieronsk',3,'',2,17),(64,'2017-01-21 20:57:21.340089','18','bieronsk',3,'',2,17),(65,'2017-01-21 21:01:39.783224','9','Client: bieronsk',1,'[{\"added\": {}}]',8,19),(66,'2017-01-21 21:05:48.951499','3','Address: Pabianicka 13',2,'[{\"changed\": {\"fields\": [\"client\"]}}]',16,19),(67,'2017-01-21 21:08:27.460956','3','Address: Pabianicka 13',2,'[{\"changed\": {\"fields\": [\"client\"]}}]',16,19),(68,'2017-01-21 21:09:07.495669','3','Address: Pabianicka 13',2,'[{\"changed\": {\"fields\": [\"client\"]}}]',16,19),(69,'2017-01-21 22:59:15.160807','4','Dish: Superdanie',1,'[{\"added\": {}}]',15,19),(70,'2017-01-22 14:26:35.633061','3','Address: Topolowa 3',2,'[{\"changed\": {\"fields\": [\"city\"]}}]',16,19);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(4,'auth','permission'),(2,'auth','user'),(5,'contenttypes','contenttype'),(16,'pycha','address'),(8,'pycha','client'),(15,'pycha','dish'),(9,'pycha','message'),(14,'pycha','opinion'),(13,'pycha','order'),(12,'pycha','parameter'),(20,'pycha','quantitydish'),(10,'pycha','restaurant'),(11,'pycha','restaurateur'),(7,'pycha','suborder'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-01-04 00:37:36.721306'),(2,'auth','0001_initial','2017-01-04 00:37:37.428132'),(3,'admin','0001_initial','2017-01-04 00:37:37.628897'),(4,'admin','0002_logentry_remove_auto_add','2017-01-04 00:37:37.649433'),(5,'contenttypes','0002_remove_content_type_name','2017-01-04 00:37:37.766917'),(6,'auth','0002_alter_permission_name_max_length','2017-01-04 00:37:37.885789'),(7,'auth','0003_alter_user_email_max_length','2017-01-04 00:37:38.001620'),(8,'auth','0004_alter_user_username_opts','2017-01-04 00:37:38.014311'),(9,'auth','0005_alter_user_last_login_null','2017-01-04 00:37:38.075372'),(10,'auth','0006_require_contenttypes_0002','2017-01-04 00:37:38.079892'),(11,'auth','0007_alter_validators_add_error_messages','2017-01-04 00:37:38.092351'),(12,'auth','0008_alter_user_username_max_length','2017-01-04 00:37:38.174363'),(13,'pycha','0001_initial','2017-01-04 00:37:40.228222'),(14,'sessions','0001_initial','2017-01-04 00:37:40.269197'),(15,'pycha','0002_auto_20170104_1031','2017-01-04 10:31:20.723590'),(16,'pycha','0002_auto_20170104_1151','2017-01-04 11:54:59.189281'),(17,'pycha','0002_type_to_parameter_address_defaults','2017-01-04 12:08:38.626325'),(19,'pycha','0003_restaurant_description','2017-01-14 11:03:02.341972'),(23,'pycha','0004_auto_20170114_1726','2017-01-14 17:51:57.673561'),(24,'pycha','0005_quantitydish_parameter','2017-01-14 17:51:59.449781');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('695pxoofelwt0jnejgr7y63929f5lm33','MDAyYjUzNjM5NTJkZmIwMTVkNWVmMjIyNDMyMjMzYjk2NjM2ZmRmZjp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1NTAzMTNjY2ExY2VjZGZiZGZlYWRhODEzMGEzZGEyYWUzZDc3OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxOSJ9','2017-02-05 14:21:18.794093'),('g0h5navn0t4ypscefzvu41qlnnc7axop','YzI4YmNiMDk4MjY5MzBjMmE0MzNhZGExNDYwMjZkMGI2MjJhMGI1Yjp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxOTNkOGNhNTRmNDI1ZTYxYTY0NWQ5OTFkMjgxOTY0MDgwNDU1MjgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-01-22 17:01:57.585779');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_address`
--

DROP TABLE IF EXISTS `pycha_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(15) NOT NULL,
  `city` varchar(30) NOT NULL,
  `post_code` varchar(6) NOT NULL,
  `street` varchar(20) NOT NULL,
  `doors` smallint(5) unsigned NOT NULL,
  `info` varchar(45) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_address_client_id_93d30018_fk_pycha_client_id` (`client_id`),
  CONSTRAINT `pycha_address_client_id_93d30018_fk_pycha_client_id` FOREIGN KEY (`client_id`) REFERENCES `pycha_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_address`
--

LOCK TABLES `pycha_address` WRITE;
/*!40000 ALTER TABLE `pycha_address` DISABLE KEYS */;
INSERT INTO `pycha_address` VALUES (1,'Polska','Wrocław','48-564','Jesienna',10,'',NULL),(2,'Polska','Kiełczów','55-093','Wrocławska',31,'',NULL),(3,'Francja','Wrocław','47-554','Topolowa',3,'Szybko przywozić mie to!',9),(4,'Polska','Wrocław','48-659','Przesztrzenna',22,'',NULL),(5,'Polska','Wrocław','35-656','Stawowa',32,'',NULL),(6,'Polska','Andrychów','78-265','Konopnickiej',19,'',4),(7,'Polska','Wrocław','48-254','Stawowa',12,'',NULL),(8,'Czechy','Wrocław','43-502','Pabianicka',12,'dssdffds',10),(9,'Niemcy','Konin','47-554','Barlickiego',13,'',11),(10,'Polska','Wrocław','50-124','Kamienskiego',17,'Ma być smaczne kurcze',13);
/*!40000 ALTER TABLE `pycha_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_client`
--

DROP TABLE IF EXISTS `pycha_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_taster` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_client_user_id_923c8b04_fk_auth_user_id` (`user_id`),
  CONSTRAINT `pycha_client_user_id_923c8b04_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_client`
--

LOCK TABLES `pycha_client` WRITE;
/*!40000 ALTER TABLE `pycha_client` DISABLE KEYS */;
INSERT INTO `pycha_client` VALUES (1,0,2),(2,0,7),(4,0,11),(5,0,12),(6,0,13),(7,0,15),(9,1,19),(10,0,20),(11,0,21),(12,0,24),(13,0,25);
/*!40000 ALTER TABLE `pycha_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_dish`
--

DROP TABLE IF EXISTS `pycha_dish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_dish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` varchar(20) NOT NULL,
  `price` decimal(7,2) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_dish_restaurant_id_8c51950d_fk_pycha_restaurant_id` (`restaurant_id`),
  CONSTRAINT `pycha_dish_restaurant_id_8c51950d_fk_pycha_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `pycha_restaurant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_dish`
--

LOCK TABLES `pycha_dish` WRITE;
/*!40000 ALTER TABLE `pycha_dish` DISABLE KEYS */;
INSERT INTO `pycha_dish` VALUES (1,'Tradycyjny schabowy','Danie główne',22.00,'Wyśmienity polski tradycyjny schabowy wraz z marchewką oraz do wyboru jeden z dodatków',1),(2,'Mielone','Danie główne',15.00,'Świetnej jakości mielony jak u babci wraz z jedną z sałatek do wyboru',1),(3,'Rumsztyk wołowy','Danie główne',26.00,'duszony w cebulce, serwowany z puree grzybowym oraz kapustą po zbójecku',2),(4,'Superdanie','Podwieczorek',50.00,'',3);
/*!40000 ALTER TABLE `pycha_dish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_dish_parameters`
--

DROP TABLE IF EXISTS `pycha_dish_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_dish_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dish_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pycha_dish_parameters_dish_id_45d4627d_uniq` (`dish_id`,`parameter_id`),
  KEY `pycha_dish_parameter_parameter_id_db176962_fk_pycha_parameter_id` (`parameter_id`),
  CONSTRAINT `pycha_dish_parameter_parameter_id_db176962_fk_pycha_parameter_id` FOREIGN KEY (`parameter_id`) REFERENCES `pycha_parameter` (`id`),
  CONSTRAINT `pycha_dish_parameters_dish_id_50caf1ee_fk_pycha_dish_id` FOREIGN KEY (`dish_id`) REFERENCES `pycha_dish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_dish_parameters`
--

LOCK TABLES `pycha_dish_parameters` WRITE;
/*!40000 ALTER TABLE `pycha_dish_parameters` DISABLE KEYS */;
INSERT INTO `pycha_dish_parameters` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,4),(5,2,5),(6,2,6),(7,3,1),(8,3,3),(9,4,2),(10,4,3);
/*!40000 ALTER TABLE `pycha_dish_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_message`
--

DROP TABLE IF EXISTS `pycha_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contents` varchar(300) NOT NULL,
  `date` datetime(6) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `sender_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_message_recipient_id_0b185a68_fk_auth_user_id` (`recipient_id`),
  KEY `pycha_message_sender_id_6226a7c8_fk_auth_user_id` (`sender_id`),
  CONSTRAINT `pycha_message_recipient_id_0b185a68_fk_auth_user_id` FOREIGN KEY (`recipient_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `pycha_message_sender_id_6226a7c8_fk_auth_user_id` FOREIGN KEY (`sender_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_message`
--

LOCK TABLES `pycha_message` WRITE;
/*!40000 ALTER TABLE `pycha_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `pycha_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_opinion`
--

DROP TABLE IF EXISTS `pycha_opinion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_opinion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` double NOT NULL,
  `contents` varchar(300) NOT NULL,
  `date` datetime(6) NOT NULL,
  `client_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_opinion_client_id_fd8ed983_fk_pycha_client_id` (`client_id`),
  KEY `pycha_opinion_restaurant_id_60e88975_fk_pycha_restaurant_id` (`restaurant_id`),
  CONSTRAINT `pycha_opinion_client_id_fd8ed983_fk_pycha_client_id` FOREIGN KEY (`client_id`) REFERENCES `pycha_client` (`id`),
  CONSTRAINT `pycha_opinion_restaurant_id_60e88975_fk_pycha_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `pycha_restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_opinion`
--

LOCK TABLES `pycha_opinion` WRITE;
/*!40000 ALTER TABLE `pycha_opinion` DISABLE KEYS */;
/*!40000 ALTER TABLE `pycha_opinion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_order`
--

DROP TABLE IF EXISTS `pycha_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_price` decimal(7,2) NOT NULL,
  `date` datetime(6) NOT NULL,
  `address_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_order_address_id_c61cdadb_fk_pycha_address_id` (`address_id`),
  KEY `pycha_order_client_id_d7cb2ac1_fk_pycha_client_id` (`client_id`),
  CONSTRAINT `pycha_order_address_id_c61cdadb_fk_pycha_address_id` FOREIGN KEY (`address_id`) REFERENCES `pycha_address` (`id`),
  CONSTRAINT `pycha_order_client_id_d7cb2ac1_fk_pycha_client_id` FOREIGN KEY (`client_id`) REFERENCES `pycha_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_order`
--

LOCK TABLES `pycha_order` WRITE;
/*!40000 ALTER TABLE `pycha_order` DISABLE KEYS */;
INSERT INTO `pycha_order` VALUES (2,37.00,'2017-01-22 14:24:26.614076',3,9);
/*!40000 ALTER TABLE `pycha_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_parameter`
--

DROP TABLE IF EXISTS `pycha_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `type` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_parameter`
--

LOCK TABLES `pycha_parameter` WRITE;
/*!40000 ALTER TABLE `pycha_parameter` DISABLE KEYS */;
INSERT INTO `pycha_parameter` VALUES (1,'Ziemniaki',1),(2,'Frytki',1),(3,'Ryż',1),(4,'Buraki',2),(5,'Colesław',2),(6,'Marchewka',2);
/*!40000 ALTER TABLE `pycha_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_quantitydish`
--

DROP TABLE IF EXISTS `pycha_quantitydish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_quantitydish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` smallint(5) unsigned NOT NULL,
  `dish_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_quantitydish_dish_id_b085111a_fk_pycha_dish_id` (`dish_id`),
  KEY `pycha_quantitydish_order_id_adb92216_fk_pycha_order_id` (`order_id`),
  KEY `pycha_quantitydish_parameter_id_51b677de_fk_pycha_parameter_id` (`parameter_id`),
  CONSTRAINT `pycha_quantitydish_dish_id_b085111a_fk_pycha_dish_id` FOREIGN KEY (`dish_id`) REFERENCES `pycha_dish` (`id`),
  CONSTRAINT `pycha_quantitydish_order_id_adb92216_fk_pycha_order_id` FOREIGN KEY (`order_id`) REFERENCES `pycha_order` (`id`),
  CONSTRAINT `pycha_quantitydish_parameter_id_51b677de_fk_pycha_parameter_id` FOREIGN KEY (`parameter_id`) REFERENCES `pycha_parameter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_quantitydish`
--

LOCK TABLES `pycha_quantitydish` WRITE;
/*!40000 ALTER TABLE `pycha_quantitydish` DISABLE KEYS */;
INSERT INTO `pycha_quantitydish` VALUES (5,1,1,2,1),(6,1,2,2,4);
/*!40000 ALTER TABLE `pycha_quantitydish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_restaurant`
--

DROP TABLE IF EXISTS `pycha_restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone` varchar(9) NOT NULL,
  `delivery_radius` smallint(5) unsigned NOT NULL,
  `address_id` int(11) NOT NULL,
  `restaurateur_id` int(11) NOT NULL,
  `description` varchar(700) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_restaurant_address_id_7587388b_fk_pycha_address_id` (`address_id`),
  KEY `pycha_restaura_restaurateur_id_6a02bd45_fk_pycha_restaurateur_id` (`restaurateur_id`),
  CONSTRAINT `pycha_restaura_restaurateur_id_6a02bd45_fk_pycha_restaurateur_id` FOREIGN KEY (`restaurateur_id`) REFERENCES `pycha_restaurateur` (`id`),
  CONSTRAINT `pycha_restaurant_address_id_7587388b_fk_pycha_address_id` FOREIGN KEY (`address_id`) REFERENCES `pycha_address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_restaurant`
--

LOCK TABLES `pycha_restaurant` WRITE;
/*!40000 ALTER TABLE `pycha_restaurant` DISABLE KEYS */;
INSERT INTO `pycha_restaurant` VALUES (1,'Jesionowo','586254986',5,1,1,'Serdecznie zapraszamy Państwa do naszej restauracji Platan, która jest wyjątkowym połączeniem tradycji z nowoczesnością. Niepowtarzalność jej wnętrza tworzą zachowane elementy starych murów oryginalnie połączone z nowoczesnym wystrojem.     Serwujemy świetną, lekką, kuchnie europejską, którą sezonowo i atrakcyjnie urozmaicamy. Zaspokoimy nawet najbardziej wyrafinowane, kulinarne oczekiwania naszych Klientów.'),(2,'Mlekiem i miodem','793265565',2,2,1,'Naszym mottem przewodnim jest „zawsze świeżo, tanio i smacznie”, dlatego postanowiliśmy stworzyć „Mlekiem i Miodem’”, które proponuje najlepszą opcję żywieniową dostępną dla wszystkich. W ofercie restauracji dostępne są m.in.: burgery, makarony, sałatki, codziennie inne, zróżnicowane dania lunch’owe, mięsne, wegetariańskie czy wegańskie, skomponowane w 100% ze świeżych i najwyższej jakości produktów.'),(3,'U Michała','502628484',3,3,2,'Description not set'),(4,'Oooobiad!','782251352',1,4,3,'Description not set'),(5,'Angularowo','513625662',3,5,3,'W stylowych i ciepłych wnętrzach naszej restauracji serwujemy dania zarówno kuchni polskiej jak i europejskiej. Nasi kucharze dbają o to, aby posiłki były nie tylko smaczne, ale również zdrowe i wykwintnie podane. Bogate menu oraz specjały naszej kuchni zadowolą nawet najbardziej wymagające podniebienia. Dla smakoszy proponujemy liczne dania wegetariańskie, rybne i sałatki. Po obfitym posiłku warto spróbować deseru. Aby umilić Państwu posiłek proponujemy szeroki wybór win.'),(6,'Kebab Ahmed','576359655',10,6,2,'Description not set'),(7,'Valdi Plus','536545415',6,7,3,'Restauracja Valdi Plus przyciąga niepowtarzalnym klimatem i doskonałym jedzeniem. Nasi kucharze oraz kelnerzy zadbają o Państwa dobry nastrój. Potrawy przyrządzane od podstaw w oparciu o naturalne i świeże produkty zaspokajają od lat podniebienia naszych gości. Doskonałym zakończeniem posiłku jest deser! Szeroki wybór ciast wykonanych według tradycyjnych receptur, deserów lodowych oraz sezonowych zadowoli każdego smakosza słodyczy.');
/*!40000 ALTER TABLE `pycha_restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_restaurateur`
--

DROP TABLE IF EXISTS `pycha_restaurateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_restaurateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_account` varchar(20) NOT NULL,
  `phone` varchar(9) NOT NULL,
  `balance` decimal(7,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_restaurateur_user_id_7851445d_fk_auth_user_id` (`user_id`),
  CONSTRAINT `pycha_restaurateur_user_id_7851445d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_restaurateur`
--

LOCK TABLES `pycha_restaurateur` WRITE;
/*!40000 ALTER TABLE `pycha_restaurateur` DISABLE KEYS */;
INSERT INTO `pycha_restaurateur` VALUES (1,'51475454485454515445','782259365',0.00,3),(2,'64864864841225154158','653535844',0.00,4),(3,'00000000000000000000','787316262',0.00,5),(4,'51564245246565656265','502654356',0.00,6),(5,'15496565251658515115','556526598',0.00,8),(6,'15343843541545341545','502658565',0.00,9),(7,'78513654847416254562','321576554',0.00,10);
/*!40000 ALTER TABLE `pycha_restaurateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_suborder`
--

DROP TABLE IF EXISTS `pycha_suborder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_suborder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(7,2) NOT NULL,
  `is_realised` tinyint(1) NOT NULL,
  `order_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pycha_suborder_order_id_bc128842_fk_pycha_order_id` (`order_id`),
  KEY `pycha_suborder_restaurant_id_524f08dd_fk_pycha_restaurant_id` (`restaurant_id`),
  CONSTRAINT `pycha_suborder_order_id_bc128842_fk_pycha_order_id` FOREIGN KEY (`order_id`) REFERENCES `pycha_order` (`id`),
  CONSTRAINT `pycha_suborder_restaurant_id_524f08dd_fk_pycha_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `pycha_restaurant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_suborder`
--

LOCK TABLES `pycha_suborder` WRITE;
/*!40000 ALTER TABLE `pycha_suborder` DISABLE KEYS */;
INSERT INTO `pycha_suborder` VALUES (3,37.00,0,2,1);
/*!40000 ALTER TABLE `pycha_suborder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pycha_suborder_dishes_quantities`
--

DROP TABLE IF EXISTS `pycha_suborder_dishes_quantities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pycha_suborder_dishes_quantities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suborder_id` int(11) NOT NULL,
  `quantitydish_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pycha_suborder_dishes_quantities_suborder_id_2022c70a_uniq` (`suborder_id`,`quantitydish_id`),
  KEY `pycha_suborder_quantitydish_id_f7eedbf0_fk_pycha_quantitydish_id` (`quantitydish_id`),
  CONSTRAINT `pycha_suborder_dishes__suborder_id_7e01cbde_fk_pycha_suborder_id` FOREIGN KEY (`suborder_id`) REFERENCES `pycha_suborder` (`id`),
  CONSTRAINT `pycha_suborder_quantitydish_id_f7eedbf0_fk_pycha_quantitydish_id` FOREIGN KEY (`quantitydish_id`) REFERENCES `pycha_quantitydish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pycha_suborder_dishes_quantities`
--

LOCK TABLES `pycha_suborder_dishes_quantities` WRITE;
/*!40000 ALTER TABLE `pycha_suborder_dishes_quantities` DISABLE KEYS */;
INSERT INTO `pycha_suborder_dishes_quantities` VALUES (5,3,5),(6,3,6);
/*!40000 ALTER TABLE `pycha_suborder_dishes_quantities` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22 17:10:41
